      subroutine greencalculscalairefft2(nx,ny,nz,nx2,ny2,nz2,nxy2,nxm
     $     ,nym,nzm,aretecube,k0,Uincx,Uincy,Uincz,FFTTENSORxx,planb)
      implicit none
      integer nx,ny,nz,nx2,ny2,nz2,nxy2,nxm,nym,nzm
      double precision aretecube,k0
      double complex Uincx,Uincy,Uincz
      double complex, dimension(8*nxm*nym*nzm) :: FFTTENSORxx
      integer ii,jj,kk,i,j,k,indice
      double precision x0,y0,z0,xx0,yy0,zz0,r
      double complex propaesplibre(3,3),icomp
      integer*8 planb
      integer FFTW_BACKWARD
      write(*,*) 'Uinc',Uincx,Uincy,Uincz
      FFTW_BACKWARD=+1

      x0=0.d0
      y0=0.d0
      z0=0.d0
      icomp=(0.d0,1.d0)
      
!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(ii,jj,kk,indice,i,j,k)
!$OMP& PRIVATE(xx0,yy0,zz0,r,propaesplibre)
!$OMP DO SCHEDULE(STATIC) COLLAPSE(3)        
      do kk=1,nz2
         do jj=1,ny2
            do ii=1,nx2
               indice=ii+nx2*(jj-1)+nxy2*(kk-1)
               if (ii.eq.nx+1.or.jj.eq.ny+1.or.kk.eq.nz+1) then
                  FFTTENSORxx(indice)=0.d0
               else
                  if (ii.gt.nx) then
                     i=(ii-1)-nx2
                  else
                     i=ii-1
                  endif
                  if (jj.gt.ny) then
                     j=(jj-1)-ny2
                  else
                     j=jj-1
                  endif
                  if (kk.gt.nz) then
                     k=(kk-1)-nz2
                  else
                     k=kk-1
                  endif
                  xx0=dble(i)*aretecube
                  yy0=dble(j)*aretecube
                  zz0=dble(k)*aretecube
                  
                  r=dsqrt(xx0*xx0+yy0*yy0+zz0*zz0)
                  if (r.eq.0.d0) r=1.d300
                  FFTTENSORxx(indice)=k0*k0*cdexp(icomp*k0*r)/r
               endif
            enddo
         enddo
      enddo
!$OMP ENDDO 
!$OMP END PARALLEL
      
      write(*,*) 'End computation of the scalar Green function'
c     compute the FFT of the Green function
  

#ifdef USE_FFTW
      call dfftw_execute_dft(planb,FFTTENSORxx,FFTTENSORxx)    
#else
      call fftsingletonz3d(FFTTENSORxx,NX2,NY2,NZ2,FFTW_BACKWARD)
#endif
      
      write(*,*) 'End FFT scalar Green function'
      end
