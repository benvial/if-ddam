\chapter{Numerical details}\label{chappola}
\markboth{\uppercase{Numerical details}}{\uppercase{Numerical details}}

\minitoc

\section{Polarizability}


The DDA discretizes the object into a set of punctual dipoles, where a
polarizability $\alpha$ is associated to each punctual dipoles. There
are different forms for this polarizability. The first to have been
used, and the simplest, is the relation of Clausius Mossotti
(CM)~\cite{Purcell_AJ_73}:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \alpha_{\rm CM} & = & \frac{3}{4\pi}\varepsilon_{\rm mul}
\frac{\varepsilon-\varepsilon_{\rm mul}}{\varepsilon+2\varepsilon_{\rm
    mul}}d^3= \varepsilon_{\rm mul} \frac{\varepsilon-\varepsilon_{\rm
    mul}}{\varepsilon+2\varepsilon_{\rm mul}}a^3, \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
where $\varepsilon$ denotes the permittivity of the object, $d$ the
size of the cubic meshsize and
$a=\left(\frac{3}{4\pi}\right)^{\frac{1}{3}}d$ the radius of the
sphere of the same volume than the cubic meshsize of the side
$d$. Unfortunately, this relation does not keep the energy and, then,
it is necessary to introduce a radiative reaction term that takes into
account the fact that charges in movement lose energy, and the
polarizability is, then, written as ~\cite{Draine_AJ_88}:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \alpha_{\rm RR} & = & \frac{\alpha_{\rm CM}}{1-\frac{2}{3} i k_0^3
  n_{\rm mul} \alpha_{\rm CM}}. \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
After different forms of the polarizability have been established in
order to improve the precision of the DDA and take into account the
non-punctual character of the dipole, and we may quote, among the best
known, the ones by Goedecke and O'Brien~\cite{Goedecke_AO_88},
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \alpha_{\rm GB} & = & \frac{\alpha_{\rm CM}}{1-\frac{2}{3} i k_0^3
  n_{\rm mul} \alpha_{\rm CM}-k_0^2 \alpha_{\rm CM}/a}, \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
by Lakhtakia~\cite{Lakhtakia_IJMPC_92}:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \alpha_{\rm LA} & = & \frac{ \alpha_{\rm CM} }{1- 2
  \frac{\varepsilon-\varepsilon_{\rm mul}}{\varepsilon+2\varepsilon_{\rm
      mul}} \left[ (1-i k_0 n_{\rm mul} a)e^{i k_0 n_{\rm mul}
      a}-1\right] } \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
and Draine and Goodman~\cite{Draine_AJ_93} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \alpha_{\rm LR} & = & \frac{ \alpha_{\rm CM}}{ 1 + \alpha_{\rm CM}
  \left[ \frac{(b_1+\varepsilon b_2/\varepsilon_{\rm mul} +\varepsilon
      b_3/\varepsilon_{\rm mul} S)k_0^2}{d}-\frac{2}{3} i n_{\rm mul}
    k_0^3 \right] },\ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
with $b_1=-1.891531$, $b_2=0.1618469$, $b_3=-1.7700004$ and $S=1/5$.

Inside the code by default, it is $\alpha_{\rm RR}$ which is used. In
the case when the permittivity is anisotropic only $\alpha_{\rm RR}$
is going to be used.

A last polarizability is introduced (PS) that only works for
homogeneous spheres and is particularly precise for metals.  This
consists of making a change of the polarizability of the elements on
the edge of the sphere taking into account the factor of
depolarization of the sphere.~\cite{Rahmani_AJ_04} Note that the
sphere should be embedded in only one layer.

\section{Solve the system of linear equation}

In order to know the electric field in the object, {\it i.e.} the
field at the position of the $N$ elements of discretization, we have
to solve the following system of linear equation:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be \ve{E} = \ve{E}_0 + \ve{A} D_\alpha \ve{E},\ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
where $\ve{E}_0$ is a vector of size $3N$ which contains the incident
field at the discretization elements. $\ve{A}$ is a matrix
$3N\times 3N$ which contains all the field tensor susceptibility and
$D_\alpha$ is a diagonal matrix $3N\times 3N$, if the object is
isotropic, or diagonal block $3\times 3$ if the object is anisotropic.
$\ve{E}$ is the vector $3N$ which contains the unknown electric local
fields. The equation is solved by a non-linear iterative method. The
code proposes numerous iterative methods, and the one used by default
is GPBICG because it is the most efficient in most cases
~\cite{Chaumet_OL_09}.  The code stops when the residue,
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\be r & = & \frac{ \|\ve{E}-\ve{A} D_\alpha \ve{E} -\ve{E}_0\|} {
  \|\ve{E}_0 \|}, \ee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
is under the tolerance given by the user. $10^{-4}$ is the tolerance
used by default, because it is a good compromise between speed and
precision. Please find below the different iterative method possible
in the code:
\begin{itemize}
\item GPBICG1 : Ref.~\onlinecite{Thuthu_IMECS_09}
\item GPBICG2 : Ref.~\onlinecite{Thuthu_IMECS_09}
\item GPBICGsafe : Ref.~\onlinecite{Fujino_IMECS_12}
\item GPBICGAR1 : Ref.~\onlinecite{Thuthu_IMECS_09}
\item GPBICGAR2 : Ref.~\onlinecite{Thuthu_IMECS_09}
\item QMRCLA : Ref.~\onlinecite{Cunha_ANM_95}
\item TFQMR : Ref.~\onlinecite{Cunha_ANM_95}
\item CG : Ref.~\onlinecite{Cunha_ANM_95}
\item BICGSTAB : Ref.~\onlinecite{Cunha_ANM_95}
\item QMRBICGSTAB1 : Ref.~\onlinecite{Chan_SIAMJSC_94}
\item QMRBICGSTAB2 : Ref.~\onlinecite{Chan_SIAMJSC_94}
\item GPBICOR : Ref.~\onlinecite{Zhao_CMA_13}
\item CORS : Ref.~\onlinecite{Carpentieri_CEI_CEIW}
\item BiCGstar-plus Ref.~\onlinecite{Fujino_WCE_15}
\end{itemize}



\section{Change of the initial guess}

When the system of linear equations is solved iteratively, we have the
possibility to choose the starting point, {\it i.e.} the initial field
$\ve{E}_i$ to start the iterative method. The closer the solution
chosen at the beginning will be close to the ``good solution'', the
more the number of iterations will be reduced. We therefore propose
the possibility to choose as initial estimate for the field:


\begin{itemize}
\item $\ve{E}_i=\ve{0}$ : null field at the beginning.
\item $\ve{E}_i=\ve{E}_0$: Born approximation.
\item Use of the scalar approximation $\ve{u}.\ve{G}\ve{u}$. In this
  case, the scalar approximation is also solved iteratively but for
  $r=0.01$. An additional precision would not be of interest, because
  we just want a correct starting point. 
\end{itemize}


\section{Preconditioning the system of linear equations}

Another solution is to precondition the matrix to be inverted on the
left to make the iterative method faster. That is to say instead of
solving $(\ve{I}-\ve{A} \ve{D}_\alpha) \ve{E}= \ve{E}_0$, we must then
solve
$\ve{P}^{-1}(\ve{I}-\ve{A} \ve{D}_\alpha) \ve{E}= \ve{P}^{-1}\ve{E}_0$
where $\ve{P}$ is a matrix close to $(\ve{I}-\ve{A} \ve{D}_\alpha)$
and whose inverse can be computed easily. For this matrix we have
chosen a matrix of Chan~\cite{Chan_SIAM_88} on the two dimensions of
space $x$ and $y$~\cite{Groth_JQSRT_20}. This preconditioning is
particularly efficient when the object under study is homogeneous or
weakly inhomogeneous and has a small thickness in $z$ compared to its
dimensions in $x$ and $y$. The preconditioning can also be also be
done on the right side, {\it i.e.} we have to find $\ve{X}$ such that
$(\ve{I}-\ve{A} \ve{D}_\alpha) \ve{P}^{-1} \ve{X}= \ve{E}_0 $, then
deduce the field with $ \ve{E}= \ve{P}^{-1}\ve{X}$.

Note that this preconditioning is also implemented for the scalar
approximation.



\section{The default options and how to change them}


The default options chosen are:

\begin{itemize}
\item The polarizability: $\alpha_{\rm RR}$.
\item The iterative method: GPBICG1.
\item The tolerance of the iterative method: $10^{-4}$.
\item The maximum number of iterations of the iterative method: 1000
\item The initial guess for the iterative method: Born approximation.
\item The preconditioning: no preconditioning.
\item  Green's function interpolation: level 2
\end{itemize}


All these options can be changed. To do this you must click on
``Advanced interface'', and appears at the bottom a xcwhole section
section called ``Numerical parameters'' where all the parameters
related to the iterative method and the polarizability can be adapted.
